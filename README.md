Ansible omicsBroker
===================

A role to install omicsBroker from https://gitlab.com/ifb-elixirfr/fair/omicsbroker

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
